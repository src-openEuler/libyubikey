Name:    libyubikey
Version: 1.13
Release: 3
Summary:  low-level C software development kit for the Yubico YubiKey authentication device
License: BSD
URL:	 https://developers.yubico.com/yubico-c/
Source0: https://developers.yubico.com/yubico-c/Releases/%{name}-%{version}.tar.gz

BuildRequires: 	gcc make

%description
This package make up the low-level C software development kit for the Yubico YubiKey authentication device.

%package devel
Summary: devel pacakge for libyubikey
Requires: %{name} = %{version}-%{release}
%description devel
This package make up the low-level C software development kit for the Yubico YubiKey authentication device.

%prep
%setup -q -n %{name}-%{version}/

%build
%configure --enable-static=no
%make_build

%install
%make_install
rm -rf %{buildroot}/%{_libdir}/*.la

%pre
%preun
%post
%postun

%check

%files
%license COPYING 
%doc AUTHORS NEWS ChangeLog README
%{_bindir}/*
%{_libdir}/libyubikey.so.*
%{_mandir}/*

%files devel
%{_includedir}/*
%{_libdir}/libyubikey.so

%changelog
* Tue Dec 13 2022 yaoxin <yaoxin30@h-partners.com> - 1.13-3
- Add make dependency

* Thu Sep 9 2021 Pengju Jiang <jiangpengju2@huawei.com> - 1.13-2
- solve the problem of safe compilation rpath

* Sun Mar 29 2020 Wei Xiong <myeuler@163.com> - 1.13-1
- Package init

